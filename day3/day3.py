import numpy as np
with open('tree', 'r') as file:
    countriesStr = file.read()
s = countriesStr.split("\n")
ma = [[i] for i in s]
m = np.array(ma)
index = 3
number = 0
row = 1
rows = len(ma)
while row < rows:
    print(row, index)
    if m[row, 0][index] == "#":
        number += 1
    index += 3
    if index >= 31:
        index = index % 31

    row += 1

print(number)