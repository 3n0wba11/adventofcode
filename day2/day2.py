import re
with open('pass', 'r') as file:
    countriesStr = file.read()
s = countriesStr.split("\n")
number = 0
def check(s):
    x = s.split(":")
    temp = re.findall(r'\d+', x[0])
    minmax = [int(i) for i in temp]
    index = s.index(":")-1
    char = s[index]
    if x[1].count(char) >= minmax[0]:
        if x[1].count(char) <= minmax[1]:
            return True
    else:
        return False

for item in s:
    if check(item):
        number+=1
print(number)

