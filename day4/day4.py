import re
parameter = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid', 'cid']
def split_on_empty_lines():
    with open('pp', 'r') as file:
        s = file.read()
    blank_line_regex = r"(?:\r?\n){2,}"
    return re.split(blank_line_regex, s.strip())

passport = split_on_empty_lines()

def check(p):
    check = False
    l = p.split("\n")
    temp = " ".join(l)
    r = temp.split(" ")
    result = [i.split(":")[0] for i in r]
    if set(result) == set(parameter):
        check = True
    temp3 = [item for item in parameter if item not in result]
    if len(temp3) == 1:
        if 'cid' in temp3:
            check = True

    return check


number = 0
for i in range(len(passport)):
    if check(passport[i]):
        number += 1
print(number)
