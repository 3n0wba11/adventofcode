import re
def split_on_empty_lines():
    with open('data', 'r') as file:
        s = file.read()
    blank_line_regex = r"(?:\r?\n){2,}"
    return re.split(blank_line_regex, s.strip())

group = split_on_empty_lines()

def number_of_yes_answer(series):
    number = 0
    answer = [" ".join(item.split("\n")) for item in series]
    for item in answer:
        if " " in item:
            new = item.split(" ")
            new = [set(item) for item in new]
            result = set.intersection(*new)
            number += len(result)
        else:
            number += len(item)
    print(number)

number_of_yes_answer(group)