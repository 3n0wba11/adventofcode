def calculate(data):
    diff_1j = 0
    diff_3j = 0
    jolt = 0
    while True:

        if jolt + 1 in data:
            diff_1j += 1
            jolt += 1
        elif jolt + 2 in data:
            jolt += 2
        elif jolt + 3 in data:
            jolt += 3
            diff_3j += 1
        else:
            break

    jolt += 3
    diff_3j += 1
    return diff_1j * diff_3j

with open('data', 'r') as file:
    countriesStr = file.read()
    data = countriesStr.split("\n")
    data = [int(i) for i in data if i.isdigit()]
    data = set(data)

print(calculate(data))