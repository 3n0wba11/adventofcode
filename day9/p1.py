with open('data', 'r') as file:
    countriesStr = file.read()
data = countriesStr.split("\n")
data = [int(i) for i in data if i.isdigit()]

def sum_of_two_number_before_it(number, index):
    number_is_available=False
    number_before_it = [int(i) for i in data[index-25:index]]
    for item in number_before_it:
        if number-item in number_before_it:
            number_is_available = True
    return number_is_available
i = 25
while i <= len(data)-1:
    number = data[i]
    if not sum_of_two_number_before_it(number, i):
        print(data[i])
    i += 1