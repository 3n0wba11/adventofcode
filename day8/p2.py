with open('data', 'r') as file:
    countriesStr = file.read()
data = countriesStr.split("\n")

def find_acc(data):
    line_position = acc = 0
    exit_program = False
    executed_lines = []
    changed_instructions = []
    current_index= 1

    while not exit_program:
        if line_position not in executed_lines:
            executed_lines.append(line_position)
            command, argument = get_instruction(line_position, data)

            if len(changed_instructions) < current_index:
                if line_position not in changed_instructions:
                    if command != 'acc':
                        changed_instructions.append(line_position)
                        if command == 'jmp':
                            command = 'nop'
                        elif command == 'nop':
                            command = 'jmp'

            line_position, acc = run_instruction(command, argument, line_position, acc, data)

            if line_position >= len(data):
                exit_program = True

        else:
            line_position = acc = 0
            executed_lines = []
            current_index += 1

    return acc

def get_instruction(line_position, data):
    command = data[line_position].split(' ')[0]
    argument = int(data[line_position].split(' ')[1])
    return command, argument


def run_instruction(command, argument, line_position, acc, data):
    if command == 'jmp':
        line_position += argument
    else:
        line_position += 1
        if command == 'acc':
            acc += argument

    return line_position, acc

print(find_acc(data))