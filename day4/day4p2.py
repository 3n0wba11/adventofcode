import re
parameter = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
eyescolor =['amb','blu','brn','gry','grn','hzl','oth']
def split_on_empty_lines():
    with open('pp', 'r') as file:
        s = file.read()
    blank_line_regex = r"(?:\r?\n){2,}"
    return re.split(blank_line_regex, s.strip())

passport = split_on_empty_lines()

def check(p):
    check = False
    l = p.split("\n")
    temp = " ".join(l)
    r = temp.split(" ")
    result = [i.split(":")[0] for i in r]
    if 'cid' in result:
        result.remove('cid')
    if set(result) == set(parameter):
        check = True
    return check

def valid(height,p):
    if p =='cm':
        if int(height) in range(150,194):
            return True
    if p =='in':
        if int(height) in range(59, 77):
            return True

def checkhcl(h):
    print(h)
    ch = False
    a = re.findall(r'#[0-9a-f]{6}', h)
    if a!=[]:
         ch = True
    return ch


def validate(p):
    che = False
    if check(p):
        l = p.split("\n")
        temp = " ".join(l)
        r = temp.split(" ")
        parameter = [i.split(":")[0] for i in r]
        value = [i.split(":")[1] for i in r]
        result = dict(zip(parameter, value))
        print(result)
        if int(result['byr']) in range(1920,2003):
            if int(result['iyr']) in range(2010,2021):
                if int(result['eyr']) in range(2020, 2031):
                    hgt = re.findall("\d+", result['hgt'])[0]
                    p = re.findall("(?!\d)\w+", result['hgt'])
                    if p != []:
                        hgt =valid(hgt, p[0])
                        if hgt:
                            hcl = checkhcl((result['hcl']))
                            if hcl:
                                if result['ecl'] in eyescolor:
                                    if len((result['pid'])) == 9:
                                        che = True

        return che


number = 0
for i in range(len(passport)):
        if validate(passport[i]):
            number += 1
print("the count equal to {}".format(number))






