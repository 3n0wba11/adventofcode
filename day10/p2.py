def search(data, jolt, memo, t):
    if jolt == t:
        return 1
    if jolt in memo:
        return memo[jolt]

    ways = 0
    if jolt + 1 in data:
        ways += search(data, jolt + 1, memo, t)
    if jolt + 2 in data:
        ways += search(data, jolt + 2, memo, t)
    if jolt + 3 in data:
        ways += search(data, jolt + 3, memo, t)
    memo[jolt] = ways
    return ways

def calculate(data, target ):
    memo = {}
    jolt = 0
    ways_to_arrange = search(data, jolt, memo, target)
    return ways_to_arrange




with open('data', 'r') as file:
    countriesStr = file.read()
    data = countriesStr.split("\n")
    data = [int(i) for i in data if i.isdigit()]
    max = max(data)
    data = set(data)

print(calculate(data,max))
