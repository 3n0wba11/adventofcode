import numpy as np
with open('tree', 'r') as file:
    countriesStr = file.read()
s = countriesStr.split("\n")
ma = [[i] for i in s]
m = np.array(ma)

def find(m, row, index):
    number = 0
    i = index
    r = row
    rows = len(ma)
    while r < rows:
        if m[r, 0][i] == "#":
            number += 1
        i += index
        if i >= 31:
            i = i % 31

        r += row
    return number

a = find(m, 1, 3)
b = find(m, 1, 1)
c = find(m, 1, 5)
d = find(m, 1, 7)
v = find(m, 2, 1)
print(a*b*c*d*v)



