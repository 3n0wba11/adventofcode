import re
def split_on_empty_lines():
    with open('data', 'r') as file:
        s = file.read()
    blank_line_regex = r"(?:\r?\n){2,}"
    return re.split(blank_line_regex, s.strip())

group = split_on_empty_lines()

def number_of_yes_answer(series):
    number = 0
    answer = [" ".join(item.split("\n")) for item in series]
    answer =[list(set(item))for item in answer]
    for item in answer:
        if " " in item:
            item.remove(" ")
        number += len(item)
    return number

print(number_of_yes_answer(group))