def traverse(key, tree):
    if key == 'no other':
        return 0
    _sum = sum([i for i in tree[key].values()])
    return _sum + sum([tree[key][i] * traverse(i, tree) for i in tree[key]])

with open('data') as _file:
    tree = {}
    for line in _file:
        rule = line.replace("bags", "").replace("bag", "").replace(".", "").rstrip().split("  contain ")
        rules = rule[1].split(", ")
        rule_dict = {}
        for color_rule in rules:
            color = ''.join(letter for letter in color_rule if letter.isalpha() or letter == " ").strip()
            number = int(''.join(num for num in color_rule if num.isdigit())) if 'no' not in color_rule else 0
            rule_dict[color] = number
        tree[rule[0]] = rule_dict
    print(traverse('shiny gold', tree))
