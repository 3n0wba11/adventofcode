with open('data', 'r') as file:
    countriesStr = file.read()
data = countriesStr.split("\n")
acc = 0
index= 0
key = data
repeat = [0 for i in data]
while index < len(data):
    if repeat[index] == 0:
        command = key[index][:3]
        repeat[index]=1

        if command == 'nop':
            index += 1

        if command =='acc':
            acc += int(key[index][3:])
            index += 1

        if command =='jmp' :
            i = int(key[index][3:])
            #print("index before jmp is {}".format(index))
            index += i
            #print("index after jmp is {}".format(index))
    else:
        print(acc)
        break